import log

from database.mariadb import mariadb
from database.rcmsdb import rcmsdb

def execute(config):
    logger = log.log(config).logger
    DB_NAME = config.get('Database', 'DB_NAME')

    try:
        #Confirm database exists.
        sysdb = mariadb(config)
        if not sysdb.IsDBexists(DB_NAME):
            logger.info("Database \'%s\' not exists. Let\'s Create it." % (DB_NAME))
            sysdb.CreateDB(DB_NAME)
        else:
            logger.info("Database \'%s\' is exists." % (DB_NAME))
        
        #Initial Tables.
        db = rcmsdb(config)
        db.Initial()
        return True
    except Exception as e:
        logger.error("Initial RCMS Database error. Becuse %s" % (e.args[0]))
        return False