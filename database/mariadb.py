import pymysql
import log

class mariadb():
    def __init__(self,config):
        self.logger=log.log(config).logger
        self.dbconfig = {
            'host':config.get('Database', 'DB_HOST'),
            'port':int(config.get('Database', 'DB_PORT')),
            'user':config.get('Database', 'DB_USER'),
            'password':config.get('Database', 'DB_PASS'),
            }
        self.dbconn = pymysql.connect(**self.dbconfig)

    def execute(self,sqlcmd,passlog=None):
        try:
            if passlog is None:
                self.logger.info("SQL Execute: %s" % (sqlcmd))
            cursor = self.dbconn.cursor()
            cursor.execute(sqlcmd)
            self.dbconn.commit()
            # if passlog is None:
            #     self.logger.info("SQL Execute okay.")
            return True
        except pymysql.Error as e:
            self.logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            raise Exception(e)

    def fetch(self,sqlcmd,passlog=None):
        try:
            if passlog is None:
                self.logger.info("SQL Execute: %s" % (sqlcmd))
            cursor = self.dbconn.cursor()
            cursor.execute(sqlcmd)
            rows = cursor.fetchall()
            if passlog is None:
                self.logger.info("Fetch okay. Result rows count: %s" % (str(len(rows))))
            return rows
        except pymysql.Error as e:
            self.logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            raise Exception(e)

    def CreateDB(self,DB_NAME):
        try:
            sqlcmd = "CREATE DATABASE IF NOT EXISTS %s default charset utf8 COLLATE utf8_general_ci" % (DB_NAME)
            self.execute(sqlcmd)
            self.logger.info("Create database %s successful." % (DB_NAME))
            return True
        except pymysql.Error as e:
            self.logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            raise Exception(e)

    def IsDBexists(self,DB_NAME):
        try:
            sqlcmd = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '%s'" % (DB_NAME)
            result = self.fetch(sqlcmd)
            if len(result) <= 0:
                return False
            else:
                return True
        except pymysql.Error as e:
            self.logger.error(" %d: %s" % (e.args[0], e.args[1]))
            raise Exception(e)