import pymysql
import log

class rcmsdb():
    def __init__(self,config):
        self.logger=log.log(config).logger
        self.dbconfig = {
            'host':config.get('Database', 'DB_HOST'),
            'port':int(config.get('Database', 'DB_PORT')),
            'user':config.get('Database', 'DB_USER'),
            'password':config.get('Database', 'DB_PASS'),
            'db':config.get('Database', 'DB_NAME'),
            }
        self.dbname = config.get('Database', 'DB_NAME')
        self.dbconn = pymysql.connect(**self.dbconfig)

    def execute(self,sqlcmd,passlog=None):
        try:
            if passlog is None:
                self.logger.info("SQL Execute: %s" % (sqlcmd))
            cursor = self.dbconn.cursor()
            cursor.execute(sqlcmd)
            self.dbconn.commit()
            # if passlog is None:
            #     self.logger.info("SQL Execute okay.")
            return True
        except pymysql.Error as e:
            self.logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            raise Exception(e)

    def fetch(self,sqlcmd,passlog=None):
        try:
            if passlog is None:
                self.logger.info("SQL Execute: %s" % (sqlcmd))
            cursor = self.dbconn.cursor()
            cursor.execute(sqlcmd)
            rows = cursor.fetchall()
            if passlog is None:
                self.logger.info("Fetch okay. Result rows count: %s" % (str(len(rows))))
            return rows
        except pymysql.Error as e:
            self.logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            raise Exception(e)

    def TableSchema(self,TableName,passlog=None):
        sqlcmd = "SELECT COLUMN_NAME,COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '%s' AND TABLE_NAME = '%s' and Extra != 'auto_increment';" % (self.dbname,TableName)
        result = self.fetch(sqlcmd,True)
        if len(result) <= 0:
            raise Exception("Cannot find Table %s." % TableName)
        else:
            if passlog is None:
                for x in range(len(result)):
                    self.logger.info("\'%s\'.\'%s\' type: %s" % (TableName,result[x][0],result[x][1]))
            return result

    def IsMachineExists(self,MachineName,passlog=None):
        sqlcmd = "SELECT Name FROM `MachineInfo` WHERE Name = '%s';" % (MachineName)
        result = self.fetch(sqlcmd,True)
        if len(result) <= 0:
            return False
        else:
            return True


    def IsTableExists(self,TableName,passlog=None):
        sqlcmd = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '%s' AND TABLE_NAME = '%s' ;" % (self.dbname,TableName)
        result = self.fetch(sqlcmd,True)
        if len(result) <= 0:
            return False
        else:
            return True

    def Initial(self):
        try:
            self.logger.info("Creating default datatable if not exists: MachineInfo .")
            sqlcmd = "CREATE TABLE IF NOT EXISTS MachineInfo ( \
                        id bigint(20) NOT NULL AUTO_INCREMENT, \
                        Name VARCHAR(60) NOT NULL, \
                        Line VARCHAR(60) DEFAULT NULL, \
                        Plant VARCHAR(60) DEFAULT NULL, \
                        Site VARCHAR(60) DEFAULT NULL, \
                        PRIMARY KEY (id) \
                        ) ENGINE=InnoDB"
            self.execute(sqlcmd,True)
            self.logger.info("Datatables are OK.")
        except pymysql.Error as e:
            self.logger.error("Error %d: %s" % (e.args[0], e.args[1]))
            raise Exception(e)