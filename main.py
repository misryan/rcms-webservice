configfile='config.conf'

#Check config file
import os
if not os.path.isfile(configfile):
    import shutil
    shutil.copy('config.sample.conf', configfile)

import configparser
config = configparser.RawConfigParser()
config.read(configfile)

#Check system enviroments
CheckEnvs=['DB_HOST','DB_PORT','DB_USER','DB_PASS','DB_NAME']
for x in range(len(CheckEnvs)):
    if not os.getenv('RCMSWS_'+CheckEnvs[x]) is None:
        config.set('Database',CheckEnvs[x],os.getenv('RCMSWS_'+CheckEnvs[x]))
config.write(open(configfile,'w'))

#Start Log system
import log
logger=log.log(config).logger

#Routes
from flask import Flask,request,jsonify,send_from_directory,redirect
app = Flask(__name__, static_url_path='/static')

# serving some static html files
@app.route('/include/<path:path>')
def send_html_include(path):
    return send_from_directory('static/include', path)

@app.route('/config')
def send_html_config():
     return redirect("/config/index.html")

@app.route('/config/<path:path>')
def send_html_config_path(path):
    return send_from_directory('static/config', path)

@app.route("/init",methods=['GET'])
def init():
    import init
    return str(init.execute(config))

@app.route("/api/putin/",methods=['POST'])
def api_putin():
    from api import putin
    content = request.json
    return jsonify(str(putin.insert(config,content)))

@app.route("/machine",methods=['GET'])
def machine_query():
    from machine import machine
    return jsonify(machine.query(config))

@app.route("/machine/<int:id>",methods=['GET'])
def machine_query_id(id):
    from machine import machine
    return jsonify(machine.query(config,id))

@app.route("/machine",methods=['POST'])
def machine_new():
    from machine import machine
    content = request.json
    return jsonify(machine.new(config,content))

@app.route("/machine/<int:id>",methods=['PUT'])
def machine_update(id):
    from machine import machine
    content = request.json
    return jsonify(machine.update(config,id,content))

@app.route("/machine/<int:id>",methods=['DELETE'])
def machine_delete(id):
    from machine import machine
    return jsonify(machine.remove(config,id))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=80)
