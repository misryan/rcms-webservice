
function renderMachineCard(result,container){
    RenderedHTML = ''
    RenderedHTML += '<div class="MachineCard">'
    RenderedHTML += '   <div class="Machine-container">'
    RenderedHTML += '       <span class="MachineName"><h2>' + result.MachineInfo.Name + '</h2></span>'
    RenderedHTML += '       <div class="MachineInfo">'
    RenderedHTML += '           <span class="MachineInfoItems">Site: <span class="MachineInfoValue">' + result.MachineInfo.Site + '</span></span>'
    RenderedHTML += '           <span class="MachineInfoItems">Plant: <span class="MachineInfoValue">' + result.MachineInfo.Plant + '</span></span>'
    RenderedHTML += '           <span class="MachineInfoItems">Line: <span class="MachineInfoValue">' + result.MachineInfo.Line + '</span></span>'
    RenderedHTML += '        </div>'
    RenderedHTML += '       <div class="MachineCardTool">'
    if (result.LastUpdate != null){
    RenderedHTML += '       <span class="UpdateTime">LastUpdate: ' + toLocaleTime(result.LastUpdate) + '</span>'
    }
    RenderedHTML += '       <a href="edit.html?id=' + result.MachineInfo.id + '" class="btn btn-info">'
    RenderedHTML += '           <span class="glyphicon glyphicon-pencil"></span>'
    RenderedHTML += '       </a>'
    RenderedHTML += '       <a href="#" class="btn btn-info" onclick="deleteMachine(' + result.MachineInfo.id + ')">'
    RenderedHTML += '           <span class="glyphicon glyphicon-trash"></span>'
    RenderedHTML += '       </a>'
    RenderedHTML += '       </div>'
    RenderedHTML += '    </div>'
    RenderedHTML += '</div>'
    container.append(RenderedHTML);
}

var MachineInfo = ['Name','Line','Plant','Site'];
var FieldType = ['varchar','int','float'];

function renderNewMachineFormTable(container_MachineInfo,container_MachineFields){

    RenderedHTML = '';
    for (i=0;i < MachineInfo.length;i++){
        RenderedHTML += '<tr>';
        RenderedHTML += '   <td class="fieldnames">' + MachineInfo[i] + '</td>';
        RenderedHTML += '   <td ><input name="MachineInfo" class="form-control"></td>';
        RenderedHTML += '</tr>';
    }
    container_MachineInfo.append(RenderedHTML);

    appendField(container_MachineFields,1)
}

function renderEditMachineFormTable(container_MachineInfo,container_MachineFields,id,Machine){

    RenderedHTML = '';
    for (i=0;i < MachineInfo.length;i++){
        RenderedHTML += '<tr>';
        RenderedHTML += '   <td class="fieldnames">' + MachineInfo[i] + '</td>';
        RenderedHTML += '   <td ><input name="MachineInfo" class="form-control" value="'+ Machine['MachineInfo'][MachineInfo[i]] +'"></td>';
        RenderedHTML += '</tr>';
    }
    container_MachineInfo.append(RenderedHTML);
    for (i=0;i< Object.keys(Machine['TableColumns']).length;i++){
        key=Object.keys(Machine['TableColumns'])[i]
        value=Machine['TableColumns'][Object.keys(Machine['TableColumns'])[i]];
        if (key.substr(0, 5)=="Data_"){
            appendField(container_MachineFields,ColumnCount+1,key,value)
        }
    }
}

var ColumnCount=0;
function appendField(container_MachineFields,id,key,value){
    key = key || "";
    value = value || "";
    var fieldType="";
    var fieldLength="";
    if (key != "" && value != ""){
        fieldType = value.split("(",1)[0];
        fieldLength = value.substring(value.lastIndexOf("(")+1,value.lastIndexOf(")"));
    }

    RenderedHTML = '';
    RenderedHTML += '<tr id="FieldName-' + id  +'">';
    RenderedHTML += '   <td class="fieldnames"><input name="FieldName"  class="form-control" placeholder="FieldName" value="' + key.replace("Data_","") + '"></td>';
    RenderedHTML += '   <td><select name="FieldType"  class="form-control">';
    for (j=0;j<FieldType.length;j++){
        if (fieldType==FieldType[j]){ 
            RenderedHTML += '   <option selected>';
        } else {
            RenderedHTML += '   <option>';
        }

        RenderedHTML += FieldType[j] + '</option>';
    }
    RenderedHTML += '   </select></td>';
    RenderedHTML += '   <td><input name="length" class="form-control" placeholder="Length" value="' + fieldLength + '"></td>';
    RenderedHTML += '   <td>';
    RenderedHTML += '       <a href="#" class="btn btn-info" onclick=\'deleteField($("#MachineFields"),'+ id +');\'> ';
    RenderedHTML += '           <span class="glyphicon glyphicon-minus"></span>';
    RenderedHTML += '       </a>';
    RenderedHTML += '   </td>';
    RenderedHTML += '</tr>';
    container_MachineFields.append(RenderedHTML);
    ColumnCount++;
}

function deleteField(container_MachineFields,id){
    $("#FieldName-" + id).remove();
    ColumnCount--;
}

function MakeMachineJSON(){
    var MachineInfoDict={};
    for (i=0;i<MachineInfo.length;i++){
        key = MachineInfo[i];
        value = document.getElementsByName("MachineInfo")[i].value;
        MachineInfoDict[key] = value;
    }

    var TableColumnsDict={};
    TableColumnsDict["Meta_Time"] = "bigint(20)";
    TableColumnsDict["Meta_SN"] = "varchar(60)";
    TableColumnsDict["Meta_Program"] = "varchar(60)";

    for (i=0;i<ColumnCount;i++){
        if (document.getElementsByName("FieldName")[i].value !=""){
            key = "Data_" + document.getElementsByName("FieldName")[i].value;

            if (document.getElementsByName("FieldType")[i].value=="varchar" || document.getElementsByName("FieldType")[i].value=="int"){
                if (document.getElementsByName("length")[i].value !=""){
                    value = document.getElementsByName("FieldType")[i].value + "(" + document.getElementsByName("length")[i].value + ")";
                    TableColumnsDict[key] = value;
                }
            }else{
                value = document.getElementsByName("FieldType")[i].value
                TableColumnsDict[key] = value;
            }
        }
    }

    var postArray={"MachineInfo": MachineInfoDict,"TableColumns":TableColumnsDict};
    return  JSON.stringify(postArray);
}

function Machine(mode,id){
    id = id || null;
    if (mode==null){return null}
    jsonContent = MakeMachineJSON();
    if (JSON.parse(jsonContent)['MachineInfo']['Name']==""){
        alert('At least we need a name.')
    }else{
        APIURL='/machine';
        if (mode=="ADD"){Method="POST";}
        else if (mode=="EDIT"){Method="PUT";APIURL += '/' + id}

        $.ajax({
            url: APIURL,
            type: Method,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            cache: false,
            data: jsonContent,
            success: function(data,status,xhr){
                if (xhr.responseJSON.status=='error'){
                    console.info('Send JSON:' + jsonContent);
                    console.error('Error Message: ' + xhr.responseJSON['message']);
                    alert ('Failed. \nPlease check configurations are reasonable.');
                }else{
                    alert ('Done.');
                    if (mode=="ADD"){location.href="/config";}
                    if (mode=="EDIT"){location.reload();}
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                alert(errorThrown);
            },
        });
    }
}

function queryMachine(id){
    var returnvalue;
    $.ajax({
        url: '/machine/' + id,
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        async: false,
        cache: false,
        success: function(data,status,xhr){
            returnvalue = data[0];
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            alert(errorThrown);
        },
    });
    while(returnvalue != undefined){
        return returnvalue;
    }
}

function deleteMachine(id){
    if (confirm('Are you sure?')){
        $.ajax({
            url: '/machine/' + id,
            type: "DELETE",
            success: function(data,status,xhr){
                alert('Done.');
                location.reload();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                alert(errorThrown);
            },
        });
    }
}

function toLocaleTime(GMTtime)
{
    if (GMTtime!='' && GMTtime!=null )
    {return moment(GMTtime).format('YYYY/MM/DD HH:mm')}
    else
    {return '';}
}