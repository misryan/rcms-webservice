import log

from database.rcmsdb import rcmsdb

def query(config,id=None):
    logger = log.log(config).logger
    try:
        db = rcmsdb(config)
        Columns = ['id','Name','Line','Plant','Site']
        if not id is None:
            sqlcmd = "select %s from `MachineInfo` where id='%s' order by %s" % (",".join(str(e) for e in Columns),id,Columns[1])
        else:
            sqlcmd = "select %s from `MachineInfo` order by %s" % (",".join(str(e) for e in Columns),Columns[1])

        result = db.fetch(sqlcmd,True)
        rows = []
        for row in result:
            MachineInfo= dict()
            MachineInfo['MachineInfo'] = dict(zip(Columns, row))
            TableSchema = dict(db.TableSchema('log_%s' % (MachineInfo['MachineInfo']['Name']),True))
            MachineInfo['TableColumns'] = TableSchema
            LastUpdate = db.fetch("select FROM_UNIXTIME(MAX(Meta_Time)) from `log_%s`" % MachineInfo['MachineInfo']['Name'],True)
            MachineInfo['LastUpdate'] = LastUpdate[0][0]
            rows.append (MachineInfo)
        return rows

    except Exception as e:
        logger.error("%s" % (e.args[0]))
        return dict({'status':'error','message':str(e.args[0])})

def new(config,jsonContent):
    j=jsonContent
    logger = log.log(config).logger
    logger.info('jsonContent: %s' % (j))

    try:
        db = rcmsdb(config)

        TableName = 'log_' + j['MachineInfo']['Name']
        
        if db.IsMachineExists(j['MachineInfo']['Name']) or db.IsTableExists(TableName):
            message="Machine name already exist."
            logger.error(message)
            return dict({'status':'error','message': message})
        else:
            logger.info("Start insert new machine information. MachineName: %s" % (j['MachineInfo']['Name']))
            values = ""
            values += "\'" + j['MachineInfo']['Name'] + "\',"
            values += "\'" + j['MachineInfo']['Line'] + "\',"
            values += "\'" + j['MachineInfo']['Plant'] + "\',"
            values += "\'" + j['MachineInfo']['Site'] + "\'"
            sqlcmd = "insert into `MachineInfo` (Name,Line,Plant,Site) values (%s)" % (values)
            db.execute(sqlcmd,True)

            logger.info("Build log table for new machine. MachineName: %s" % (j['MachineInfo']['Name']))
            Columns = []
            Columns.append("id bigint(20) NOT NULL AUTO_INCREMENT")
            for x in range(len(j['TableColumns'])):
                ColumnName = list(j['TableColumns'].keys())[x]
                Type = j['TableColumns'][ColumnName]
                Columns.append ("%s %s DEFAULT NULL" % (ColumnName,Type))
            Columns.append("PRIMARY KEY (id)")
            ColumnsStr = ",".join(str(e) for e in Columns)
            sqlcmd = "CREATE TABLE IF NOT EXISTS `%s` (%s) ENGINE=InnoDB" % (TableName,ColumnsStr)
            db.execute(sqlcmd,True)

            message="All done. You can start log now. MachineName: %s" % (j['MachineInfo']['Name'])
            logger.info(message)
            return dict({'status':'info','message': message})

    except Exception as e:
        logger.error("%s" % (e.args[0]))
        return dict({'status':'error','message':str(e.args[0])})

def update(config,MachineId,jsonContent):
    j=jsonContent
    logger = log.log(config).logger
    logger.info('jsonContent: %s' % (j))
    try:
        db = rcmsdb(config)
        
        sqlcmd=""
        MachineName = (db.fetch("select Name from MachineInfo where id=%s" % (MachineId),True))[0][0]
        if (not MachineName == j['MachineInfo']['Name']):
            sqlcmd="RENAME TABLE `log_%s` TO `log_%s`;" % (MachineName,j['MachineInfo']['Name'])
            db.execute(sqlcmd,True)
            logger.info("Table renamed `log_%s` -> `log_%s`" % (MachineName,j['MachineInfo']['Name']))
            MachineName=j['MachineInfo']['Name']


        logger.info("Start update machine information. MachineName: %s" % (MachineName))
        setstr = ""
        setstr += "Name=\'" + j['MachineInfo']['Name'] + "\',"
        setstr += "Line=\'" + j['MachineInfo']['Line'] + "\',"
        setstr += "Plant=\'" + j['MachineInfo']['Plant'] + "\',"
        setstr += "Site=\'" + j['MachineInfo']['Site'] + "\'"
        sqlcmd = "update `MachineInfo` set %s where id=%s" % (setstr,MachineId)
        
        if not sqlcmd == "":
            db.execute(sqlcmd,True)

        sqlcmd=""
        logger.info("Start update tableschema. MachineName: %s" % (j['MachineInfo']['Name']))
        OrgTableSchema =dict(db.TableSchema('log_' + MachineName,True))
        NewTableSchema = j['TableColumns']
        
        for field in OrgTableSchema:
            if field in NewTableSchema:
                if (not NewTableSchema[field]==OrgTableSchema[field]):
                    sqlcmd += "ALTER TABLE `%s` MODIFY %s %s;" % ('log_' + MachineName,field,NewTableSchema[field])
            else:
                sqlcmd += "ALTER TABLE `%s` DROP COLUMN %s;" % ('log_' + MachineName,field)

        for field in NewTableSchema:
            if not field in OrgTableSchema:
                sqlcmd += "ALTER TABLE `%s` ADD %s %s;" % ('log_' + MachineName,field,NewTableSchema[field]) 
        
        if not sqlcmd == "":
            db.execute(sqlcmd,True)

        message="All done. MachineName: %s" % (MachineName)
        logger.info(message)
        return dict({'status':'info','message': message})

    except Exception as e:
        logger.error("%s" % (e.args[0]))
        return dict({'status':'error','message':str(e.args[0])})

def remove(config,MachineId):
    logger = log.log(config).logger
    try:
        db = rcmsdb(config)
        sqlcmd = "select Name from `MachineInfo` where id = %s" % (MachineId)
        result = db.fetch(sqlcmd,True)
        if len(result) > 0:
            logger.info("Deleting Machine : %s" % (result[0][0]))
            sqlcmd = "drop table `log_%s`" % (str(result[0][0]))
            db.execute(sqlcmd,True)
            sqlcmd = "delete from `MachineInfo` where id = %s" % (MachineId)
            db.execute(sqlcmd,True)
        else:
            message="Cannot find the machine you want to delete. id: %s" % (MachineId)
            logger.error(message)
            return dict({'status':'error','message': message})
        
        message="Machine deleted. id: %s" % (MachineId)
        logger.info(message)
        return dict({'status':'info','message': message})
    except Exception as e:
        logger.error("%s" % (e.args[0]))
        return dict({'status':'error','message':str(e.args[0])})