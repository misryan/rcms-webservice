import logging
import logging.config

class log():
    def __init__ (self,config):
        self.LogConfigPath=config.get('Logger', 'LogConfigPath') 
        self.LoggerName=config.get('Logger', 'LoggerName') 
        logging.config.fileConfig(self.LogConfigPath)
        self.logger=logging.getLogger(self.LoggerName)