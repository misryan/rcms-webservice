import log

from database.rcmsdb import rcmsdb

import datetime

def insert(config,jsonContent):
    logger = log.log(config).logger
    logger.info('jsonContent: %s' % (jsonContent))

    try:
        db = rcmsdb(config)

        #MachineName
        MachineName=list(jsonContent.keys())[0]

        #Query TableSchema
        TableName = 'log_' + MachineName
        TableSchema = db.TableSchema(TableName,True)
        ColumnsList = list(list(zip(*TableSchema))[0])

        #Mapping JSON values
        insertValues = []
        for x in range(len(TableSchema)):
            columnname = TableSchema[x][0]
            if columnname.startswith('Meta_') or columnname.startswith('Data_'):
                ColumnSplited = columnname.split('_',1)
                try:
                    value=str(list(jsonContent.values())[0][ColumnSplited[0]][ColumnSplited[1]])
                    if TableSchema[x][1].startswith('varchar'):
                        value = "\'"+ value + "\'"
                    insertValues.append(value)
                except:
                    ColumnsList.remove(columnname)
                    pass
        
        # DB Insert
        Columns = ",".join(str(e)for e in ColumnsList)
        values = ",".join(str(e)for e in insertValues)
        sqlcmd="insert into `%s` (%s) values (%s)" % (TableName,Columns,values)
        return db.execute(sqlcmd)

    except Exception as e:
        logger.error("Insert data error. Becuse %s" % (e.args[0]))
        return False