#!/bin/bash

version=$1
username=docker.io/misryan
imagename=rcms-webservice

docker build -t $username/$imagename:latest -f Dockerfile .
docker tag $username/$imagename:latest $username/$imagename:$version